from mido import MetaMessage, Message, MidiFile, MidiTrack
import numpy.random as rd
import numpy as np
import midi2audio as m2a
import os
from shutil import copyfile
from pathlib import Path
import pygame

def rand_ev(low, high, nstp, seed=None, loc=0, scale=1):
    """Generate a sequence following a random evolution using gaussian noise

    Parameters
    ----------
    low: int, minimal value of the sequence
    high: int, maximal value
    nstp: int, number of elements in the sequence
    seed: int, initial value, if none specified choose a random int between
    low and high
    loc: float, the mean value of the gaussian noise
    scale: float, the standard deviation

    Output
    ------
    rands: ints list, the entire sequence between low and high of size nstp

    Comments
    --------
    This function is the heart of quantic music
    """
    if not seed:
        seed = rd.randint(low, high)  # Generate an integer seed using a uniform law

    rands = []
    for i in range(nstp):
        seed += int(rd.normal(loc, scale))  # Generate the next value using a normal law

        # The two bounds are sticky seed cannot go beyond them.
        if seed < low:
            rands += [low]
            seed = low
        elif seed > high:
            rands += [high]
            seed = high
        else:
            rands += [seed]  # Add the int to the list

    return rands



def set_ibs(mid, ib_nuance, ib_ns, trk=None, ib_init=None, tk_per_beat=480):
    """"Set the interbeat intervals (ibs) of a song using rand_v to provide a unique texture to it

    Parameters
    ----------
    ib_nuance: couple of floats, minium and maximum interbeat interval
    ib_ns: list of int triples, (number of beats, mean value of the noise, its standard deviation)
    ib_init: float, an initial ib
    tck_per_beat: int, number of ticks per beat
    """
    ib_min = ib_nuance[0]
    ib_max = ib_nuance[1]
    if not ib_init:
        ibs = [rd.randint(ib_min, ib_max)]
    else:
        ibs = [ib_init]

    for c_ib in ib_ns:
        ibs += rand_ev(ib_min, ib_max, c_ib[0], ibs[-1], c_ib[1], c_ib[2])

    if not trk:  # Create a track to have all the temporal variation
        trk = MidiTrack()
        trk.name = "Tempo variation"

    # Set a metamessage to change the tempo between each beat
    trk.append(MetaMessage("set_tempo",
                           tempo=ibs[0],
                           time=0))

    for i, c_ib in enumerate(ibs[1:]):
        trk.append(MetaMessage("set_tempo",
                               time=tk_per_beat,
                               tempo=c_ib))

    mid.tracks.append(trk)

    return mid, ibs



def velocity(nuance, notes, vel_init=None):
    """Generate a drifting volume within a given range"""
    if not vel_init:
        vel_init = int(rd.normal(loc=np.mean(nuance)))

    vel = []
    for i, note in enumerate(notes):
        if i == 0:
            sd = vel_init
        else:
            sd = vel[-1]
        vel += rand_ev(nuance[0], nuance[1], note[0], seed=sd, loc=note[1])

    return vel


def char2note(char):
    """Convert a char to a midi note number wih the followung syntax. First
    the note ('a', 'b' , etc..) followed by the keyboard octave number and
    finally its accentuation ('-', '+'). By default the on velocity and off
    velocity are set"""
    if char == 's':  # Return a silence
        return 0, 0, 0
    notes = {'a': 69, 'b': 71, 'c': 60, 'd': 62, 'e': 64, 'f': 65, 'g': 67}
    note_nb = notes[char[0]]
    note_nb += (int(char[1])-4) * 12
    if len(char) == 3:
        if char[2] == '-':
            note_nb -= 1
        if char[2] == '+':
            note_nb += 1
    v_on = 64
    v_off = 100
    return note_nb, v_on, v_off


def split(val, div, scale=1):
    """Divide in two in a random fashion using a normal distribution"""
    a_remainer = - 1
    b_remainer = - 1
    while a_remainer < 0 or b_remainer < 0:
        a_remainer = int(rd.normal(loc=val*div, scale=scale))
        b_remainer = val - a_remainer

    return a_remainer, b_remainer



def notes2trk(notes, trk=None, lght=(1, 0)):
    """Create a track from notes with a particular syntax."""
    if not trk:
        trk = MidiTrack()
    remain = 0 #There is a problem when you put multiple trk in series

    for i, note in enumerate(notes):
        if note[0] == 's':  # Message for a silence
            trk.append(Message("note_on", note=0, velocity=0, time=0))
            trk.append(Message("note_off", note=0, velocity=0, time=note[1]))
            continue

        nlist = note[0].split(' ')

        for i, c_n in enumerate(nlist):  # Add all notes as on type message
            n_nb, v_pn, v_off = char2note(c_n)
            trk.append(Message("note_on", note=n_nb, velocity=note[2],
                               time=remain))
            remain = 0
        end, remain = split(note[1], lght[0], scale=lght[1])

        trk.append(Message("note_off", note=n_nb, velocity=100,
                           time=end))

        for c_n in nlist[:-1]:  # Add the note off message
            n_nb, v_pn, v_off = char2note(c_n)
            trk.append(Message("note_off", note=n_nb, velocity=100,
                               time=0))

    return trk



def merge(notes, duration, volume, trk=None):
    """"Merge the different element to add a track to a midi file"""
    if (len(notes) != len(duration)) or (len(notes) != len(volume)):
        print(len(notes), len(duration), len(volume))
        # import pdb; pdb.set_trace()  # XXX BREAKPOINT

    l_size = max(len(notes), len(duration), len(volume))
    out = [[notes[i], duration[i], volume[i]] for i in range(l_size)]
    trk = notes2trk(out, trk)
    return trk




def pedal_t(mid, times, rand=True):
    """Create a pedal track on and off at given times"""
    trk = MidiTrack()
    trk.name = "Pedal"
    flip_on = True
    for t in times:
        if rand:
            p_on = rd.randint(64, 127)
            p_off = rd.randint(0, 64)
        else:
            p_on = 127
            p_off = 0
        if flip_on:
            trk.append(Message("control_change", control=64, value=p_on,
                               time=t))
            flip_on = False
        else:
            trk.append(Message("control_change", control=64, value=p_off,
                               time=t))
            flip_on = True
    mid.tracks.append(trk)
    return mid




def ibs2t(ibs, ib_per_bt=4):
    """Produce the time vector corresponding to change in ib"""
    t = []
    tp = 0
    t.append(tp)
    for c_ib in ibs[:-1]:
        tp += c_ib
        t.append(tp/(ib_per_bt*10**3))
    return t


def ibs2tpo(ibs):
    """Turn interbeat interval into a tempo or the other ways around"""
    return (1000000/ibs)*60


def play_music(midi_filename):
  '''Stream music_file in a blocking manner'''
  set_pygame()
  clock = pygame.time.Clock()
  pygame.mixer.music.load(midi_filename)
  pygame.mixer.music.play()
  while pygame.mixer.music.get_busy():
    clock.tick(30) # check if playback has finished


def set_pygame():
    # mixer config
    freq = 44100  # audio CD quality
    bitsize = -16   # unsigned 16 bit
    channels = 2  # 1 is mono, 2 is stereo
    buffer = 1024   # number of samples
    pygame.mixer.init(freq, bitsize, channels, buffer)

    # optional volume 0 to 1.0
    pygame.mixer.music.set_volume(0.8)


def play(midi_filename):
    # listen for interruptions
    try:
        # use the midi file you just saved
        play_music(midi_filename)
    except KeyboardInterrupt:
        # if user hits Ctrl/C then exit
        # (works only in console mode)
        pygame.mixer.music.fadeout(1000)
        pygame.mixer.music.stop()
        raise SystemExit


if __name__ == "__main__":
    trk = None
    mid = MidiFile()
    bt = mid.ticks_per_beat
    # Add the scale to a mid file
    key = ["c3 c4", "d3 d4", "e3 e4", "f3 f4", "g3 g4", "a3 a4", "b3 b4",
           "c4 c5"]
    # Set a constant duration of a Whole here
    dur = [4*bt for s in key]

    # Vary the velocity/volume for each key stroke
    vol = velocity([0, 127], [[8, -2.9]])

    # Merge all and add to the mid
    lght = input("choose how light your hand is (float between 0 and 1):")
    out = [[key[i], dur[i], vol[i]] for i in range(len(key))]
    trk = notes2trk(out, trk, (float(lght), 20))
    # trk = merge(key, dur, vol, trk)
    mid.tracks.append(trk)

    # Make the four beats interval evolve from 500000
    mid, ibs = set_ibs(mid, [300000, 700000], [[7*4+3, 0, 1000]], ib_init=500000)
    mid.save("lib.mid")

    # Generat the mid file
    play("lib.mid")
    #mid.print_tracks()
