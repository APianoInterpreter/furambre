# The idea behind quantic music
The regular beat constitutes music' essence.
Musicians learn to play using a regular metronome and computers use perfectly
adjusted tempos to play MIDI files. Live music, however, doesn't use regular
tempo, and musicians learn to pace themselves without a metrone. With quantic
music I want to reproduce this almost organic regular tempo. To do that, I
introduce noise to change the duration between each beats, but not any kind of
noise. I use a brownian or what I call a drifting noise: I add or substract 200
ms betwee, each beat for instance. I do the same with the strength at which the
computer play each note. Quantic music is not a new style of music, it is a new
way to perform music. With Quantic music I want to design a walkman capable of
generating each time a unique interpretation of a musical piece.

# Install
Start by cloning this git depo.

You then need Python 3+, Mido, Numpy and Pygame. To install all the depedencies simply type within a shell 

     pip install -r REQUIRELENTS.txt 

You may need to install freepats to render the generated mid file

    sudo apt-get install freepats

Finally to generate an interpretation of Fur Elise by Beethoven:

    python FurElise.py

# To know more
Visit my [Soundcloud page](https://soundcloud.com/theunknownscientist) to hear more quantic music. And look at my [Youtube channel](https://www.youtube.com/watch?v=Jw5IEunPIlE) to vizualize interpretations.
